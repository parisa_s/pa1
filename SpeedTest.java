/**
*SpeedTest is a class for run Tasktimer.
* @author Parisa Supitayakul.
*/
public class SpeedTest {
	/**
	 *Used to run TaskTimer with tasks(arrayList) .
	 */
	public static void main(String [] args) {
		TaskTimer testTime = new TaskTimer();
		Runnable[] tasks = {new Task1(),new Task2(),new Task3(),new Task4(),new Task5()};
		
		for(int i=0;i<tasks.length;i++) {
			testTime.measureAndPrint(tasks[i]);
		}
	}
}

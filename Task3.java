/**
*Task3 is a class that used for test speed by add double primitives from an array.
* @author Parisa Supitayakul.
*/
public class Task3 implements Runnable{
	private int counter ;
	private double[] values;
	private int ARRAY_SIZE = 500000;
	/**
	 * Constructor for declared array list of values and counter.
	 */
	public Task3() {
		counter = 100000000;
		values = new double[ARRAY_SIZE];
		for(int k=0; k<ARRAY_SIZE; k++) {
			values[k] = k+1;
		}
	}
	/**
	 * run is a method for add double primitives from an array.
	 */
	public void run() {
		double sum = 0.0;
		// count = loop counter, i = array index value
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}
		
	}
	/** 
     * Returns a string description of Task3 and count.
     * @return String with count.
     */
	public String toString() {
		return String.format("task3:Sum array of double primitives with count=%,d\n", counter);
	}
}

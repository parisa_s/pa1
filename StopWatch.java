/**
*StopWatch that measure elapsed time.
* @author Parisa Supitayakul.
*/
public class StopWatch {
	
	private long timeStart;
	private long timeStop;
	/**
	 * Constructor for create StopWatch and declared timeStart,timeStop.
	 */
	public StopWatch() {
		timeStart = 0;
		timeStop = 0;
	}
	/**
	 *getElapsed return elapsed time in seconds.
	 *@return elapsed time since start until current time, if StopWatch is running .
	 *        elapsed time between start to stop ,if StopWatch is not running.
	 */
	public double getElapsed() {
		double elapsed;
		if(isRunning()) {
			elapsed = (System.nanoTime() - timeStart)*Math.pow(10, -9);
		}
		else {
			elapsed = (timeStop - timeStart)*Math.pow(10, -9);
		}
		return elapsed;
	}
	/**
	 *For check StopWatch is running or not.
	 *@return true if StopWatch is running, false if it is not running.
	 */
	public boolean isRunning() {
		boolean isRun = false ;
		if(timeStop==0 && timeStop - timeStart!=0) {
			isRun = true;
		}
		
		return isRun;
	}
	/**
	 *For start time of StopWatch.
	 */
	public void start() {
		if(!isRunning()) {
			timeStart = System.nanoTime();
		}
	}
	/**
	 *For stop time of StopWatch.
	 */
	public void stop() {
		if(isRunning()) {
			timeStop = System.nanoTime();
		}
	}
}

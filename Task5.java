import java.math.BigDecimal;

/**
*Task5 is a class that used for test speed by  add BigDecimal objects from an array.
* @author Parisa Supitayakul.
*/
public class Task5 implements Runnable{
	private int counter ;
	private int ARRAY_SIZE = 500000;
	private BigDecimal[] values;
	/**
	 * Constructor for declared array list of values and counter.
	 */
	public Task5() {
		counter = 100000000;
		values = new BigDecimal[ARRAY_SIZE];
		for(int i=0; i<ARRAY_SIZE; i++) {
			values[i] = new BigDecimal(i+1);
		}
	}
	/**
	 * run is a method for  add BigDecimal objects from an array.
	 */
	public void run() {
		BigDecimal sum = new BigDecimal(0.0);
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum.add( values[i] );
		}
	}
	/** 
     * Returns a string description of Task5 and count.
     * @return String with count.
     */
	public String toString() {
		return String.format("task5:Sum array of BigDecimal with count=%,d\n", counter);
	}
}

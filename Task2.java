/**
*Task2 is a class that used for test speed by append chars to a StringBuilder.
* @author Parisa Supitayakul.
*/
public class Task2 implements Runnable{
	private StringBuilder builder;
	private int counter ;
	private final char CHAR = 'a';
	/**
	 * Constructor for declared StringBuilder and counter.
	 */
	public Task2() {
		builder = new StringBuilder(); 
		counter = 100000;
	}
	/**
	 * run is a method for append chars to a StringBuilder.
	 */
	public void run() {
		int k = 0;
		while(k++ < counter) {
			builder = builder.append(CHAR);
		}
	}
	/** 
     * Returns a string description of Task2 and count.
     * @return String with count.
     */
	public String toString() {
		return String.format("task2:Append to StringBuilder with count=%,d\n", counter);
	}
}

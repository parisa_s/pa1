#StopWatch by Parisa Supitayakul (5710546313)
I ran on Windows 7 64 bit , and got these results :

##Why is there such a big difference in the time used to append chars to a String and to a StringBuilder?
This is a table that show the time when used difference between task1 and task2.

                  Task                                              |     Time

Append 100,000 char to String                                                  | 5.549705 sec

Append 100,000 char to StringBuilder   	                                   | 0.004739 sec

####Explaination of result:
StringBuilder is faster than String because StringBuilder can modify word ,so it mustn't create new string(word) like String.


##Why is there a significant difference in times to sum double, Double, and BigDecimal values?
This is table that show the time of task3-5.

                   Task                     		                |     Time

Sum 100,000,000 array of double primitives                                                       | 0.135364 sec

Sum 100,000,000 array of Double objects                                                          | 0.681819 sec

Sum 100,000,000 array of BigDecimal                                                                | 1.577746 sec

####Explaination of result:
double primitives is faster than Double objects because it doesn't create an object before caluculate (Double objects will create object first) and BigDecimal is slowest because it bigger than other ,it can keep large object.
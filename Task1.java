/**
*Task1 is a class that used for test speed by append chars to a string.
* @author Parisa Supitayakul.
*/
public class Task1 implements Runnable{
	private int counter ;
	private final char CHAR = 'a';
	/**
	 * Constructor for create Task1 and declared counter.
	 */
	public Task1() {
		counter = 100000;
	}
	/**
	 * run is a method for append chars to a string.
	 */
	public void run() {
		String sum = ""; 
		int k = 0;
		while(k++ < counter) {
			sum = sum + CHAR;
		}
	}
	/** 
     * Returns a string description of Task1 and count.
     * @return String with count.
     */
	public String toString() {
		return String.format("task1:Append to String with count=%,d\n", counter);
	}
}

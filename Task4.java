/**
*Task4 is a class that used for test speed by add Double objects from an array.
* @author Parisa Supitayakul.
*/
public class Task4 implements Runnable{
	private int counter ;
	private double[] values;
	private int ARRAY_SIZE = 500000;
	/**
	 * Constructor for declared array list of values and counter.
	 */
	public Task4() {
		counter = 100000000;
		values = new double[ARRAY_SIZE];
		for(int i=0; i<ARRAY_SIZE; i++) {
			values[i] = new Double(i+1);
		}
	}
	/**
	 * run is a method for add Double objects from an array.
	 */
	public void run() {
		Double sum = new Double(0.0);
		// count = loop counter, i = array index
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}
		
	}
	/** 
     * Returns a string description of Task4 and count.
     * @return String with count.
     */
	public String toString() {
		return String.format("task4:Sum array of Double objects with count=%,d\n", counter);
	}
}

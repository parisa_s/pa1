/**
*TaskTimer is a class that used for measure time of each task 
*and print result time.
* @author Parisa Supitayakul.
*/
public class TaskTimer {
	private static double elapsed;
	/**
	 * Constructor for create TaskTimer.
	 */
	public TaskTimer() {
		
	}
	/**
	 *measureAndPrint is a method for measure time and print result.
	 *@param runnable is an interface that each task implements.
	 */
	public static void measureAndPrint(Runnable runnable) {
		StopWatch watch = new StopWatch();
		System.out.println (runnable.toString());
		
		watch.start();
		runnable.run();
		watch.stop();
		elapsed = watch.getElapsed();
		
		System.out.printf("Elapsed time %.6f sec\n\n", elapsed);
	}
}
